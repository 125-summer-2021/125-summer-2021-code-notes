// count_vowels.cpp

#include <iostream>

using namespace std;

int main() {
	char c;
	int space_count = 0;
	int vowel_count = 0;
	while (cin.get(c)) {
		switch (c) {
			case ' ':
				space_count++;
				break;
			case 'a': case 'A': 
			case 'e': case 'E':
			case 'i': case 'I': 
			case 'o': case 'O': 
			case 'u': case 'U': 
				vowel_count++; 
		} // switch
	} // while

	cout << "Number of spaces: " << space_count << "\n";
	cout << "Number of vowels: " << vowel_count << "\n";
} // main