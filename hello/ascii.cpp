// ascii.cpp

#include <iostream>
#include <iomanip>

using namespace std;

const int a_code    = int('a');
const int A_code    = int('A');
const int zero_code = int('0');

int main() {
	// upper case letters
	for(int i = 0; i < 13; i++) {
		cout << char(i+A_code)    << setw(4) << (i+A_code) 
		     << "    "
		     << char(i+A_code+13) << setw(4) << (i+A_code+13)
			 << "\n";
	}

	cout << "\n";

	// lower case letters
	for(int i = 0; i < 13; i++) {
		cout << char(i+a_code)    << setw(4) << (i+a_code) 
		     << "    "
		     << char(i+a_code+13) << setw(4) << (i+a_code+13)
			 << "\n";
	}

	cout << "\n";

	// digits
	for(int i = 0; i < 5; i++) {
		cout << char(i+zero_code)   << setw(4) << (i+zero_code) 
		     << "    "
		     << char(i+zero_code+5) << setw(4) << (i+zero_code+5)
			 << "\n";
	}

} // main
