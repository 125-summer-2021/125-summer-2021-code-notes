// recursion.cpp

#include <iostream>

using namespace std;

// void a(const string& s, int max, int n) {
// 	if (n > max) {
// 		return;
// 	} else {
// 		cout << n << ": " << s << "\n";
// 		a(s, max, n+1);
// 	}
// }

void b(const string& s, int n) {
	if (n <= 0) {
		return;
	} else {
		cout << n << ": " << s << "\n";
		b(s, n-1);
	}
}

void count_down(int n) {
	if (n <= 0) {  // base case
		return;
	} else {       // recursive case
		cout << n << "\n";
		count_down(n-1);
	}
}

void count_up(int n) {
	if (n <= 0) {  // base case
		return;
	} else {       // recursive case
		count_up(n-1);
		cout << n << "\n";
	}
}

int S(int n) {
	if (n == 0) {
		return 0;
	} else {
		return n + S(n-1);
	}
}

long F(long n) {
	if (n == 1) {
		return 1;
	} else if (n == 2) {
		return 1;
	} else {
		return F(n-2) + F(n-1);
	}
}

int main() {
	for(int i = 1; i <= 50; i++) {
		cout << F(i) << "\n";
	}
}
