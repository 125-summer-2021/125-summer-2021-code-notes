// enums.cpp

#include <iostream>
#include <string>

using namespace std;

enum class Color { 
	red, green, blue 
};

// string to_str(Color c) {
// 	switch (c) {
// 		case Color::red   : return "red";
// 		case Color::green : return "green";
// 		case Color::blue  : return "blue";
// 		default           : return "unknown";
// 	}
// }

string to_str(Color c) {
	if (c == Color::red)        return "red";
	else if (c == Color::green) return "green";
	else if (c == Color::blue)  return "blue";
	else                        return "unknown";
}


int main() {
	Color a = Color::blue;
	int red = 5;
	// cout << a << "\n";   // error: can't print enum value directly
	cout << int(a) << "\n"; // prints 2
	cout << to_str(a) << "\n";
}
