// struct1.cpp

#include <iostream>

using namespace std;

struct Point {
	double x;
	double y;
}; // <-- don't forget this semi-colon!

struct Circle {
	Point center;
	double radius;
}; // <-- don't forget this semi-colon!

int main() {
	Point p{1.2, 3.4};
    cout << p.x << " " << p.y << "\n";
    p.x = 0;
    p.y = 5;
    cout << p.x << " " << p.y << "\n";

	Circle c{{5, 10}, 6.2};
	cout << "center: " 
	     << c.center.x << " "
	     << c.center.y << "\n"
	     << "radius: " << c.radius
	     << "\n";
}
