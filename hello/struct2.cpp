// struct2.cpp

#include <iostream>
#include <string>

using namespace std;

struct Person {
	string first_name;
	string last_name;
	int age;
}; // <-- don't forget this semi-colon!

int main() {
	Person a{"Margaret", "Atwood", 81};
    string full_name = a.first_name + " " + a.last_name;
    cout << full_name << " is " << a.age << " years young\n";
}
