// count.cpp

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	// initialize the variables we need
	vector<string> words;
	string s;

	// read one word at a time from cin until there are no more words
	while (cin >> s) {
		words.push_back(s);
	}

	// sort all the words in alphabetical order
	sort(begin(words), end(words));

	for(string s : words) cout << s << "\n";

	cout << words.size() << " words\n";
}