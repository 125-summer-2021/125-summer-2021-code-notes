// primes3.cpp

//
// Based on primes2.cpp: uses a while loop to test which numbers in a range
// are prime.
//
// An integer n is a prime number if it is greater than 1, and has exactly two
// different divisors, 1 and n. The first few primes are 2, 3, 5, 7, 11, ...,
// Note that 2 is the only even prime number.
//

#include <iostream>

using namespace std;

// Returns true if n is a prime number, and false otherwise.
bool is_prime(int n) {
	if (n < 2) {             // numbers less than 2 are not prime
		return false;
	} else if (n == 2) {     // 2 is the smallest prime number
		return true;
	} else if (n % 2 == 0) { // all other even numbers are not prime
		return false;
	} else {
		// At this point we know n is an odd positive number. To test if it's
		// prime, we check to see if any of the odd numbers 3, 5, 7, ... up to
		// the square root of n are divisors of n. If none of them do, then n
		// must be prime.
		//
		// Note that this is *not* the most effcient way to test if a number
		// is prime, but it is good enough for our purposes.
		int trial_divisor = 3;
		while (trial_divisor * trial_divisor <= n) {
			if (n % trial_divisor == 0) {
				return false;
			}
			trial_divisor += 2;
		}
		return true;
	}
}

int main() {
    const int MAX = 1000;

	int n = 1;
	int count = 0;
	while (n < MAX) {
		if (is_prime(n)) {
			cout << n << "\n";
			count++;
		}
		n++;
	}

	cout << count << " primes from 1 to " << MAX << "\n";
} // main
