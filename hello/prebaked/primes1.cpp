// primes1.cpp

//
// Tests if an integer entered by the user is prime number.
//
// An integer n is a prime number if it is greater than 1, and has exactly two
// different divisors, 1 and n. The first few primes are 2, 3, 5, 7, 11, ...,
// Note that 2 is the only even prime number.
//

#include <iostream>

using namespace std;

int main() {
	cout << "Enter an integer: ";
	int n;
	cin >> n;

	if (n < 2) {
		cout << n << " is NOT a prime\n";
	} else if (n == 2) {
		cout << n << " is a prime\n";
	} else if (n % 2 == 0) {
		return 0;
	} else {
		int trial_divisor = 3;
		while (trial_divisor * trial_divisor <= n) {
			if (n % trial_divisor == 0) {
				cout << n << " is NOT a prime\n";
				return 0;
			}
			trial_divisor += 2;
		}
		cout << n << " is a prime\n";
	}
} // main
