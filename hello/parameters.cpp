// parameters.cpp

#include <iostream>
#include <string>

using namespace std;

// pass by value: a copy of s is given to pluralize1
string pluralize1(string s) {
	int n = s.size();
	if (s[n-1] == 'y') {
		s[n-1] = 'i';
		return s + "es";
	} else {
		return s + "s";
	}
}

// pass by reference: s is the actual passed-in parameter
//
// This can be faster and use less memory than pass by value (because no copy
// is needed). But, it changes s, which almost certainly not be what you want!
string pluralize2_bug(string& s) {
	int n = s.size();
	if (s[n-1] == 'y') {
		s[n-1] = 'i';
		return s + "es";
	} else {
		return s + "s";
	}
}

// pass by reference: s is the actual passed-in parameter
//
// pluralize3 doesn't return a value, but instead modifies the pass-in string.
// A disadvantage of pluralize3 is that it doesn't work with string literals,
// e.g. calling  pluralize3("puppy") causes a compile-time error.
void pluralize3(string& s) {
	int n = s.size();
	if (s[n-1] == 'y') {
		s[n-1] = 'i';
		s += "es";
	} else {
		s += "s";
	}
}

// pass by constant reference: s is the actual passed-in parameter, and C++
// does not allow s to be modified anywhere in the body of the function.
//
// This is often the best way to pass parameters: it's fast because no copy is
// made, and C++ guarantees it is not modified inside the function. Plus it
// works with literals, e.g. quote("puppy") returns the string "puppy".
string quote(const string& s) {
	return "\"" + s + "\"";
}

int main() {
	string s = "puppy";
	cout << "s = " << quote(s) << "\n";
	// cout << pluralize2_bug(s) << "\n";
	pluralize3(s);
	cout << "s = " << quote(s) << "\n";
	cout << quote("puppy") << "\n";
}
