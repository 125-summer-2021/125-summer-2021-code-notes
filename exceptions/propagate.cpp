// propagate.cpp

#include "cmpt_error.h"
#include <iostream>
#include <string>

using namespace std;

void c() {
    cout << "c() called ...\n";
    cmpt::error("problem!!!");
    cout << "... c() ended normally\n";
}

void b() {
    cout << "b() called ...\n";
    c();
    cout << "... b() ended normally\n";
}

void a() {
    cout << "a() called ...\n";
    b();
    cout << "... a() ended normally\n";
}

// crashes the program because the exception thrown by c propagates and is
// never caught
void example1() {
    cout << "example1() called ...\n";
    a();
    cout << "... example1() ended normally\n";   
}

// the exception thrown by c propagates and is caught, so the program ends
// with a friendly error message
void example2() {
    cout << "example2() called ...\n";
    try {
        a();
    } catch (...) {
        cout << "caught an exception!\n";
    }
    cout << "... example2() ended normally\n";   
}

int main() {
    // example1();
    example2();
}
