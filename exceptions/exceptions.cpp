// exceptions.cpp

#include "cmpt_error.h"
#include <iostream>
#include <string>

using namespace std;

// Given a string s of the form "a + b", where a and b are ints, returns their
// sum.
int eval(const string& s) {
    // find the position of the '+'
    int plus_loc = s.find('+');
    if (plus_loc == string::npos) cmpt::error("eval: + not found");

    // extract a and b as their own strings (s.substr(i, n) returns a new
    // string of length n that is a copy of s[i] up to s[i+n-1])

    // " 23 + 4"
    string a = s.substr(0, plus_loc);  // a == " 23 "
    string b = s.substr(plus_loc + 1, s.size() - plus_loc); // b == " 4"

    // convert a and b to ints using the standard function stoi (string to
    // int)
    int result = stoi(a) + stoi(b);

    return result;
}

void example1() {
	cout << eval("2 + 3")         << "\n";  // 5
	cout << eval("25+-3")         << "\n";  // 22
	cout << eval(" 8   +  10   ") << "\n";  // 18
}

void example2() {
	// eval throws an exception because "two" isn't an int
	cout << eval(" two + 3");
}

void example3() {
	// eval throws an exception because the second int is too big
	cout << eval(" 2 + 39043090300473");
}

void print_safe(const string& s) {
    try {
        int result = eval(s);
        cout << "result = " << result << "\n\n";
    } catch (const std::invalid_argument& e) {
        cout << "error: one, or both, of the operands of \"" << s << "\"\n"
             << "       is not a valid int\n\n";
    } catch (const std::out_of_range& e) {
        cout << "error: one, or both, of the operands of \"" << s << "\"\n"
             << "       are outside the range of an int\n\n";    
    } catch (...) { // catches any exception
        cout << "error: an unknown error has occurred\n\n";
    }
}

void example4() {
	print_safe(" 8   +  10   ");
	print_safe(" two + 3");
	print_safe(" 2 + 39043090300473");
    print_safe(" 2  3");
}

int main() {
	// example1();
	// example2();
	// example3();
	example4();
}
