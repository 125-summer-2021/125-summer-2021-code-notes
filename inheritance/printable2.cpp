// printable2.cpp

#include "cmpt_error.h"
#include <iostream>
#include <vector>

//
// This code is based in printable1.cpp, and uses a base class called
// Printable that Point, Person, and Reading inherit from. This means that
// Point, Person, and Reading *don't* need to define their own copy of
// println, but instead can use the one inherited from Printable.
//

using namespace std;

class Printable {
public:
    // It's necessary to define the print() method here since it is used in
    // the println method that follows. It is "= 0" to indicate that it has no
    // default implementation, and it is "virtual" to indicate that a class
    // that inherits from Printable can provide it's own instance of print().
    virtual void print() const = 0;

    // prints the object to cout followed by "\n"
    void println() const {
        print();
        cout << "\n";
    }

    // Always add a virtual destructor to a base class so that inheriting
    // classes can implement their own destructor if they need to.
    virtual ~Printable() { }
}; // class Printable

class Point : public Printable {
private:
    double x;
    double y;

public:
    // default constructor
    Point() : x(0), y(0) { }

    // copy constructor
    Point(const Point& other) : x(other.x), y(other.y) { }

    Point(double a, double b) : x(a), y(b) { }

    // getters
    double get_x() const { return x; }
    double get_y() const { return y; }

    void print() const {
        cout << '(' << x << ", " << y << ')';
    }
}; // class Point


class Person : public Printable {
    string name;
    int age;
public:
    Person(const string& n, int a)
    : name{n}, age{a}
    {
        if (age < 0) cmpt::error("negative age");
    }

    string get_name() const { return name; }
    int get_age() const { return age; }

    void print() const {
        cout << "Name: '" << name << ", Age: " << age;
    }
}; // class Person


class Reading : public Printable {
private:
    string loc;
    double temp;
public:
    Reading(const string& l, double t)
    : loc{l}, temp{t}
    { }

    string get_loc() const { return loc; }
    double get_temp() const { return temp; }

    void print() const {
        cout << temp << " degrees at " << loc;
    }
}; // class Reading


void example1() {
    Point a{1, 2};
    a.println();

    Person b{"Katja", 22};
    b.println();

    Reading c{"backyard", 2.4};
    c.println();
}

void example2() {
    Point a{1, 2};
    Person b{"Katja", 22};
    Reading c{"backyard", 2.4};

    // Printable p;  // impossible

    Printable* p = &a;
    p->print();
    p->println();

    // p = &b;
    // p->print();
    // p->println();

    // things contains only Printable* pointers, but those pointers point to
    // different types of objects. It's *almost* as if we have a vector
    // containing different types of objects.
    vector<Printable*> things = {
        &a, &b, &c
    };

    // We don't know at compile-time which println is called ... it depends on
    // the type of the object p points to.
    // for(Printable* p : things) {
    //     p->println();
    // }
    for(int i = 0; i < things.size(); i++) {
        things[i]->println();
    }
}

int main() {
    // example1();
    example2();
}
