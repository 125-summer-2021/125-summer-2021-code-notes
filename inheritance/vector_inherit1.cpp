// vector_inherit1.cpp

//
// Creates a new class named int_vec that inherits from vector<int>.
//

#include <iostream>
#include <vector>

using namespace std;

class int_vec : public vector<int> {  // int_vec inherits from vector<int>

	// nothing yet!

}; // class int_vec

int main() {
	int_vec v;

	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	for(int n : v) cout << n << "\n";
}
