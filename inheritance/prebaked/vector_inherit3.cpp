// vector_inherit3.cpp

//
// Adds a sort method to the int_vec class from vector_inherit2.cpp.
//

#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

using namespace std;

class int_vec : public vector<int> {
public:
	int sum() const {
		return std::accumulate(begin(), end(), 0);
	}

	// sort is a standard C++ function for sorting vectors and arrays.
	// std::sort(begin(), end()) sorts the entire vector in order from
	// smallest to largest.
	// 
	// The begin() and end() methods are inherited from vector<int>, and so we
	// are free to use them in int_vec methods.
	void sort_increasing() {
		std::sort(begin(), end());
	}

}; // class int_vec

int main() {
	int_vec v;

	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	v.sort_increasing();
	for(int n : v) cout << n << "\n";
	cout << "sum = " << v.sum() << "\n";
}
