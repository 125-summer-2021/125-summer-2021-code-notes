// vector_inherit2.cpp

//
// Adds a sum method to the int_vec class from vector_inherit1.cpp.
//

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class int_vec : public vector<int> {
public:
	// accumulate is a standard C++ function for summing vectors and arrays.
	// std::accumulate(begin(), end(), 0) sums the values from the start of
	// the vector to the end, starting with an initial value of 0.
	// 
	// The begin() and end() methods are inherited from vector<int>, and so we
	// are free to use them in int_vec methods.
	int sum() const {
		return std::accumulate(begin(), end(), 0);
	}

}; // class int_vec

int main() {
	int_vec v;

	v.push_back(4);
	v.push_back(5);
	v.push_back(1);
	
	for(int n : v) cout << n << "\n";
	cout << "sum = " << v.sum() << "\n";
}
