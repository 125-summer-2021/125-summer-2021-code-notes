// color.cpp

#include <iostream>
#include <string>

using namespace std;

class Color {
public:
	int red   = 0;
	int green = 0;
	int blue  = 0;

	string to_string() const {
		return "rgb(" + std::to_string(red)   + ", " 
		              + std::to_string(green) + ", " 
		              + std::to_string(blue)  + ")";
	}
};

class Named_color : public Color {
public:
	string name;

	string to_string() const {
		return name + "=" + Color::to_string();
	}
};

int main() {
	Named_color c1{255, 215, 0, "gold"};
	cout << c1.to_string() << "\n";
}
