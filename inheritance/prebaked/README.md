# Examples of Inheritance

## Ineriting from a vector<int>

- [vector_inherit1.cpp](vector_inherit1.cpp): Creates a new class named
  `int_vec` that inherits from `vector<int>`.

- [vector_inherit2.cpp](vector_inherit2.cpp): Adds a sum method to the
  `int_vec` class from [vector_inherit1.cpp](vector_inherit1.cpp).

- [vector_inherit3.cpp](vector_inherit3.cpp): Adds a sort method to the
  `int_vec` class from [vector_inherit2.cpp](vector_inherit2.cpp).

- [vector_inherit4.cpp](vector_inherit5.cpp): Adds a name member variable (and
  getter) to the `int_vec` class from
  [vector_inherit2.cpp](vector_inherit2.cpp).

- [vector_inherit5.cpp](vector_inherit5.cpp): Adds `summarize` and
  `fancy_summarize` functions (not methods) to
  [vector_inherit4.cpp](vector_inherit4.cpp).

## Ineriting from a Base Class

- [printable1.cpp](printable1.cpp): Shows three separate classes (`Point`,
  `Person`, and `Reading`) that are *not* related through inheritance, but all
  have the same `println` method.

- [printable2.cpp](printable2.cpp): The three classes from
  [printable1.cpp](printable1.cpp) inherit `println` from the same base class
  `Printable`. `Printable` includes a virtual abstract method `print` that is
  necessary for `println` to be inherited.

## Virtual Destructors

- [virtual_destructor.cpp](virtual_destructors/virtual_destructor.cpp):
  Provides sample code that demonstrates why virtual destructors are
  necessary. The [virtual_destructor/makefile](virtual_destructor/makefile)
  has some compiling options turned off so the incorrect code can run.
