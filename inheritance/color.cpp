// color.cpp

#include <iostream>
#include <string>

using namespace std;

class Color {
public:
	int red;
	int green;
	int blue;

	virtual ~Color() { }

	Color(int r, int g, int b)
	: red(r), green(g), blue(b)
	{ }

	virtual string to_string() const {
		return "RGB(" + std::to_string(red)   + ", " 
		              + std::to_string(green) + ", " 
		              + std::to_string(blue)  + ")";
	}
};

class Named_color : public Color {
public:
	string name;

	Named_color(const string& cname, int r, int g, int b)
	: Color(r, g, b), name(cname)
	{ }

	string to_string() const {
		return name + "=" + Color::to_string();
	} 
};

int main() {
	Color c{255, 0, 0}; // red
	cout << c.to_string() << " (c is a Color)\n";

	// Named_color nc{"gold", 255, 215, 0};
	// cout << nc.to_string() << " (nc is a Named_color)\n";

	Color* cp = new Color{255, 0, 0};
	cout << cp->to_string() << "\n";

	Named_color* ncp = new Named_color{"gold", 255, 215, 0};
	cout << ncp->to_string() << " (ncp points to a Named_color)\n";

	delete cp;

	cp = ncp;
	cout << cp->to_string() << " (cp points to a Named_color)\n";

	delete ncp;
} // main



	// Color* cp = new Color{255, 0, 0};
	// cout << cp->to_string() << "\n";

	// Named_color* ncp = new Named_color{"gold", 255, 215, 0};
	// cout << ncp->to_string() << " (ncp points to a Named_color)\n";

	// delete cp;

	// cp = ncp;
	// cout << cp->to_string() << " (cp points to a Named_color)\n";

	// delete ncp;

