// sfml_test_mouse.cpp

//
// A simple mouse-drawing program.
//

#include <SFML/Graphics.hpp>


int main() {
    sf::RenderWindow window(sf::VideoMode(500, 500), 
                            "SFML Drawing");
    window.setFramerateLimit(60);

    // 
    // Use a red circle for the pen.
    //
    sf::CircleShape pen(5);
    pen.setFillColor(sf::Color::Red);

    //
    // main animation loop; or main event loop
    //
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        //
        // The window is *not* cleared because we want the pen markings to
        // stay on the screen.
        //

        //
        // get the mouse position
        //
        sf::Vector2i mouse = sf::Mouse::getPosition(window);
        //pen.setPosition(mouse.x, mouse.y);

        //
        // when the user clicks the left mouse button, draw on the screen
        //
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            pen.setPosition(mouse.x, mouse.y);
            window.draw(pen);
        }

        // 
        // re-draw the screen
        //
        window.display();
    }

    return 0;
}
