// linear_search.cpp

#include <iostream>
#include <vector>

using namespace std;

// Pre-condition:
//    none
// Post-condition:
//    Returns true if x occurs one or more times in v, and 
//    false otherwise.
bool contains1(const vector<int>& v, int x) {
    for(int i = 0; i < v.size(); i++) {
        if (v[i] == x) {
            return true;    // success: found x
        }
    }
    return false;           // failure: didn't find x
}

// Pre-condition:
//    none
// Post-condition:
//    Returns the smallest i >= 0 such that v[i] == x; or, if
//    x is not anywhere in v, then -1 is returned
int index1(const vector<int>& v, int x) {
    for(int i = 0; i < v.size(); i++) {
        if (x == v[i]) return i;
    }
    return -1;
}

bool contains2(const vector<int>& v, int x) {
    return index1(v, x) != -1;
}

bool contains3(const vector<int>& v, int x) {
    if (index1(v, x) == -1) {
        return false;
    } else {
        return true;
    }
}

// linear search on the range [begin, end)
int index2(const vector<int>& v, int x, int begin, int end) {
    for(int i = begin; i < end; ++i) {
        if (x == v[i]) return i;
    }
    return -1;
}

int index3(const vector<int>& v, int x) {
    return index2(v, x, 0, v.size());
}

// recursive linear search on a range
int index3(const vector<int>& v, int x, int begin, int end) {
    if (begin >= end) {
        return -1;
    } else if (v[begin] == x) {
        return begin;
    } else {
        return index3(v, x, begin + 1, end);  // note the + 1
    }
}

// recursive linear search on a range
int index4(const vector<int>& v, int x) {
    return index3(v, x, 0, v.size());
}

int main() {
  vector<int> v = {5, 4, 3, 82, 2, 1, 0, 12, 2};
  for(int x : v) cout << x << " ";
  
  cout << "\n\n";
  cout << "contains1(v, 2)) = " << contains1(v, 2) << "\n";
  cout << "contains2(v, 2)) = " << contains2(v, 2) << "\n";
  cout << "   index1(v, 2)) = " << index1(v, 2)    << "\n";
  cout << "   index3(v, 2)) = " << index3(v, 2)    << "\n";
  cout << "   index4(v, 2)) = " << index4(v, 2)    << "\n";

  cout << "\n";
  cout << "contains1(v, 6)) = " << contains1(v, 6) << "\n";
  cout << "contains2(v, 6)) = " << contains2(v, 6) << "\n";
  cout << "   index1(v, 6)) = " << index1(v, 6)    << "\n";
  cout << "   index3(v, 6)) = " << index3(v, 6)    << "\n";
  cout << "   index4(v, 6)) = " << index4(v, 6)    << "\n";
}
