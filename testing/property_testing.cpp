// property_testing.cpp

#include "cmpt_error.h"
#include <iostream>
#include <string>
#include <vector>
#include <cassert>

using namespace std;

string trim(const string& s) {
    int first = s.find_first_not_of(' ');
    if (first == string::npos) return "";
    int last = s.find_last_not_of(' ');
    return s.substr(first, last - first + 1);
}

// These properties hold for any string s. 
// Don't need to create outputs!
void test_properties(const string& s) {
    assert(trim(" " + s) == trim(s));
    assert(trim(s + " ") == trim(s));
    assert(trim(" " + s + " ") == trim(s));
    assert(trim(trim(s)) == trim(s));
}

int main() {
    vector<string> inputs = {
        "", " ", "  ", 
        "a", " a", "a ", " a ",
        "ab", " ab", "ab ", " ab "
    };

    int test_count = 0;
    for(string s : inputs) {
        test_properties(s);
        test_count += 4;
    }

    cout << test_count << " tests passed!\n";
}
