// puzzle.cpp

#include <iostream>

using namespace std;

int main() {
	int** a = new int*(nullptr);
	*a = new int(3);

	cout << **a << "\n";  // 3
	**a = 5;
	cout << **a << "\n";  // 5
	
	delete *a;
	delete a;
}
