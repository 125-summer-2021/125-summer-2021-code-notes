// memory.cpp

#include <iostream>
#include <vector>
#include <string>

using namespace std;

void example1() {
	double* x = new double(12.1);
	cout << " x: " << x << "\n";
	cout << "*x: " << *x << "\n";
	delete x;
}

void example2() {
	const int N = 5;
	double* arr = new double[N];
	for(int i = 0; i < N; i++) {
		cout << "arr[" << i << "] = " << arr[i] << "\n";
	}
	delete[] arr;  // delete[] used to delete arrays
}

double* make_arr(int size, double init = 0.0) {
	double* result = new double[size];
	for(int i = 0; i < size; i++) {
		result[i] = init;
	}
	return result;
}

void print(double* arr, int size) {
	for(int i = 0; i < size; i++) {
		cout << "arr[" << i << "] = " << arr[i] << "\n";
	}
}

double sum(double* arr, int size) {
	double result = 0.0;
	for(int i = 0; i < size; i++) {
		result += arr[i];
	}
	return result;
}

void example3() {
	const int N = 5;
	double* arr = make_arr(N, 6.25);
	print(arr, N);
	cout << "Total: " << sum(arr, N) << "\n";
	delete[] arr;  // delete[] used to delete arrays
}

double* double_the_size(double* arr, int size) {
	// create the new array
	double* new_arr = new double[2*size];

	// copy the old array into the new one
	for(int i = 0; i < size; i++) {
		new_arr[i] = arr[i];
	}

	// de-allocate the old array
	delete[] arr;

	// set the new positions all to 0.0
	for(int i = size; i < 2*size; i++) {
		new_arr[i] = 0.0;
	}	

	return new_arr;
}

void example4() {
	// N is *not* const because it will change later
	int N = 5;
	double* arr = make_arr(N, 6.25);

	print(arr, N);
	cout << "Total: " << sum(arr, N) << "\n";

	arr = double_the_size(arr, N);
	N *= 2; // size of arr has doubled

	cout << "\n";
	print(arr, N);
	cout << "Total: " << sum(arr, N) << "\n";

	delete[] arr;  // delete[] used to delete arrays
}

int main() {
	example4();
	cout << "done\n";
}
