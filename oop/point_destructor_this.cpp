// point_destructor_this.cpp

#include "cmpt_error.h"
#include <iostream>
#include <string> 

using namespace std;

struct Point {
    double x    = 0;  // member initialization
    double y    = 0;  // i.e. x and y are set to 0 by default
    string name = "no name";

    // default constructor
    Point()                                // x and y are initialized above
    { }

    Point(double a, double b, const string& n)
    : x(a), y(b), name(n)                  // initializer list
    { }

    // copy constructor
    Point(const Point& other)
    : Point(other.x, other.y, other.name)  // constructor delegation
    { }

    // destructor
    ~Point() {
        cout << "Point: " << name << " destructor called\n";
    }

    // Note the use of the "this" pointer to access the member variables of
    // this class.
    string to_str() const {
        return "(" + to_string(this->x) + ", "
                   + to_string(this->y) + ", "
                   + this->name
                   + ")";
    }

    void print() const {
        cout << to_str();
    }

    void println() const {
        print();
        cout << "\n";
    }

}; // struct Point

int main() {
    Point origin;
    Point p{2, -4, "p"};
    p.x = 0;
    origin.println();
    p.println();
}
