// point_friend.cpp

#include "cmpt_error.h"
#include <iostream>

using namespace std;

class Point {
    double x;                  // x and y are private
    double y;
public:
    Point(double a, double b)  // constructor
    : x(a), y(b)
    { }

    double get_x() const { return x; }
    double get_y() const { return y; }

    void print() const { cout << x << ", " << y << "\n"; }
    
    // The set_to_origin function is a friend of this class, i.e. it can
    // access private members of Point.
    friend void set_to_origin(Point& p);
};

void set_to_origin(Point& p) {   // friend function
    p.x = 0.0;
    p.y = 0.0;
}

void point1() {
    Point p(10, -3.4);
    p.print();
    set_to_origin(p);
    p.print();
}

int main() {
    point1();
}
