// count_chars8.cpp

//
// The same as count_chars7.cpp, but now the code for processing the file is
// written as a method inside Count.
//
// You use it like this:
//
//     $ ./count_chars8
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
    ifstream infile;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // constructor
    Count(const string& fname) 
    : infile(fname)  // initialization list
    { }

    void print_results() {
        cout << "#chars: " << chars << "\n";
        cout << "#lines: " << lines << "\n";
        cout << "#tabs : " << tabs  << "\n";
        cout << "#words: " << words << "\n";
    }

    // process infile one character at a time, incrementing the appropriate
    // count variables
    void process_file() {
        char c;
        bool last_char_whitespace = true;
        while (infile.get(c)) {
            chars++;
            switch (c) {
                case '\n': lines++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case '\t': tabs++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case ' ' : if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                default  : last_char_whitespace = false;
            } // switch
        } // while
    } // process_file
}; // Count


int main() {
    Count num("austenPandP.txt");
    num.print_results();
    num.process_file();
    
    count.words = -100;
    cout << "Average words per line: " 
         << count.words / count.lines << "\n";
}
