// count_chars4.cpp

//
// The same as count_chars3.cpp, but instead of reading the file from cin the
// file is read using an ifstream object.
//
// You use it like this:
//
//     $ ./count_chars4
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
   int chars = 0;
   int lines = 0;
   int tabs  = 0;
   int words = 0;
};

void print_results(const Count& num) {
    cout << "#chars: " << num.chars << "\n";
    cout << "#lines: " << num.lines << "\n";
    cout << "#tabs : " << num.tabs  << "\n";
    cout << "#words: " << num.words << "\n";
}

int main() {
    ifstream infile("austenPandP.txt");

    // initialize the count object
    Count num;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    char c;
    bool last_char_whitespace = true;
    while (infile.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    print_results(num);
}
