// count_chars2.cpp

//
// Fixes the word over-counting bug in count_chars1.cpp. 
//
// It does this by using a boolean varaible to track if the last character was
// a whitespace character. The word count is only incremented at a whitespace
// character is preceded by a non-whitespace character. This stops multiple
// whitespace characters in a row from being counted as multiple words.
//
// The file is read from cin, so you use it like this:
//
//     $ ./count_chars2 < austenPandP.txt
//

#include <iostream>

using namespace std;

int main() {
    // initialize the count variables
    int num_chars = 0;
    int num_lines = 0;
    int num_tabs  = 0;
    int num_words = 0;

    // process cin one character at a time, incrementing the appropriate count
    // variables
    char c;
    bool last_char_whitespace = true;
    while (cin.get(c)) {
        num_chars++;
        switch (c) {
            case '\n': num_lines++;
                       if (!last_char_whitespace) num_words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num_tabs++;
                       if (!last_char_whitespace) num_words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num_words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false; // default catches all other characters
                       // break is not needed on last case
        } // switch
    } // while

    // report the results
    cout << "#chars: " << num_chars << "\n";
    cout << "#lines: " << num_lines << "\n";
    cout << "#tabs : " << num_tabs  << "\n";
    cout << "#words: " << num_words << "\n";
}
