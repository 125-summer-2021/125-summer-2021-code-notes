// count_chars9.cpp

//
// Based on count_chars8.cpp, but now its easier to use. The programmer just
// creates a Count object, and then calls the getters. process_file is called
// inside the constructor, and so the programmer doesn't need to worry about
// calling it.
//
// The variables have been made private so the user can't accidentally (or
// intentionally!) change them. The user can only access them through the
// getters.
//
// The process_file method has been made private to ensure it is only called
// once in the constructor.
// 
// You use it like this:
//
//     $ ./count_chars9
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
private:
    ifstream infile;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    void process_file() {
        char c;
        bool last_char_whitespace = true;
        while (infile.get(c)) {
            chars++;
            switch (c) {
                case '\n': lines++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case '\t': tabs++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case ' ' : if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                default  : last_char_whitespace = false;
            } // switch
        } // while
    } // process_file

public:
    // constructor
    Count(const string& fname) 
    : infile(fname)  // initialization list
    { 
        process_file();
    }

    int num_chars() const {
        return chars;
    }

    int num_lines() const {
        return lines;
    }

    int num_tabs() const {
        return tabs;
    }

    int num_words() const {
        return words;
    }

    void print_results() const {
        cout << "#chars: " << num_chars() << "\n";
        cout << "#lines: " << num_lines() << "\n";
        cout << "#tabs : " << num_tabs()  << "\n";
        cout << "#words: " << num_words() << "\n";
    }
}; // Count


int main() {
    Count count("austenPandP.txt");
    char c;
    cin >> c;
    if (c == 'y') {
        count.print_results();
        cout << "Average words per line: " 
             << count.num_words() / count.num_lines() << "\n";
    }
}
