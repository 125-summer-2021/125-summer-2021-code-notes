// count_chars3.cpp

//
// Replaces the count variables of count_chars2.cpp with a struct. Also added
// a function called print_results to make main a little simpler.
//
// The file is read from cin, so you use it like this:
//
//     $ ./count_chars3 < austenPandP.txt
//

#include <iostream>

using namespace std;

struct Count {
   int chars = 0;
   int lines = 0;
   int tabs  = 0;
   int words = 0;
};

void print_results(const Count& num) {
    cout << "#chars: " << num.chars << "\n";
    cout << "#lines: " << num.lines << "\n";
    cout << "#tabs : " << num.tabs  << "\n";
    cout << "#words: " << num.words << "\n";
}

int main() {
    // initialize the count object
    Count num;

    // process cin one character at a time, incrementing the appropriate count
    // variables
    char c;
    bool last_char_whitespace = true;
    while (cin.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    print_results(num);
}
