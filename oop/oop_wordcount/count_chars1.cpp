// count_chars1.cpp

//
// Counts the number of characters, lines, tabs, and words in a file. The file
// is read from cin, so you use it like this:
//
//     $ ./count_chars1 < austenPandP.txt 
//     #chars: 704158
//     #lines: 13427
//     #tabs : 0
//     #words: 127342
//
// However, there is a slight bug in how words are counted. Every space causes
// the word count to be incremented, and so the word count is actually a count
// of the number of spaces. If there's more than one whitespace character
// between words, then this can over-count the number of words. We see that
// this is indeed the case when we use the standard word count utility wc to
// count the number of words:
//
//     $ wc -w austenPandP.txt 
//     124580 austenPandP.txt
//
// The program below says there are 127342 words, which is small but
// non-trivial over-counting.
//

#include <iostream>

using namespace std;

int main() {
    // initialize the count variables
    int num_chars = 0;
    int num_lines = 0;
    int num_tabs  = 0;
    int num_words = 0;

    // process cin one character at a time, incrementing the appropriate count
    // variables
    char c;
    while (cin.get(c)) {
        switch (c) {
            case '\n': num_chars++;
                       num_lines++;
                       num_words++;
                       break; // break is needed to prevent fallthrough
            case '\t': num_chars++;
                       num_tabs++;
                       break; // break is needed to prevent fallthrough
            case ' ' : num_chars++;
                       num_words++;
                       break; // break is needed to prevent fallthrough
            default  : num_chars++; // default case catches all other characters
                                    // break is not needed on last case
        } // switch
    } // while

    // report the results
    cout << "#chars: " << num_chars << "\n";
    cout << "#lines: " << num_lines << "\n";
    cout << "#tabs : " << num_tabs  << "\n";
    cout << "#words: " << num_words << "\n";
}
