// count_chars10.cpp

//
// The same as count_chars9.cpp, but it has been made *lazy*. Instead of
// calling process_file in the constructor, process_file is now called in the
// getters *if* necessary.
//
// This can improve the performance of Count objects. If you create a Count
// object but then never use any of its getters, then the file is never read.
// 
// The implementation of Count now includes a boolean varaible called
// is_processed that keeps track of whether or not process_file has been
// called. Each getter checks it before returning its value.
//
// While this does make the implementation of the Count a little more
// complicated, the *use* of Count is exactly the same as before. The user
// just creates a Count object and calls its getters, and they don't need to
// worry about the implementation details.
//
// You use it like this:
//
//     $ ./count_chars10
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
private:
    ifstream infile;
    bool is_processed = false;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    void process_file() {
        char c;
        bool last_char_whitespace = true;
        while (infile.get(c)) {
            chars++;
            switch (c) {
                case '\n': lines++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case '\t': tabs++;
                           if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                case ' ' : if (!last_char_whitespace) words++;
                           last_char_whitespace = true;
                           break;
                default  : last_char_whitespace = false;
            } // switch
        } // while
        
        is_processed = true;
    } // process_file

public:
    // constructor
    Count(const string& fname) 
    : infile(fname)  // initialization list
    { }

    int num_chars() {
        if (!is_processed) process_file();
        return chars;
    }

    int num_lines() {
        if (!is_processed) process_file();
        return lines;
    }

    int num_tabs() {
        if (!is_processed) process_file();
        return tabs;
    }

    int num_words() {
        if (!is_processed) process_file();
        return words;
    }

    void print_results() {
        cout << "#chars: " << num_chars() << "\n";
        cout << "#lines: " << num_lines() << "\n";
        cout << "#tabs : " << num_tabs()  << "\n";
        cout << "#words: " << num_words() << "\n";
    }
}; // Count


int main() {
    Count count("austenPandP.txt");
    count.print_results();
    cout << "Average words per line: " 
         << count.num_words() / count.num_lines() << "\n";
}
