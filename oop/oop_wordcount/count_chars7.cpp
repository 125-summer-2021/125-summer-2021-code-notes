// count_chars7.cpp

//
// The same as count_chars6.cpp, but with a constructor added to Count so that
// different files can be read in.
//
// You use it like this:
//
//     $ ./count_chars7
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
    ifstream infile;

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    // constructor
    Count(const string& fname) 
    : infile(fname) // initialization list
    { }

    void print_results() {
        cout << "#chars: " << chars << "\n";
        cout << "#lines: " << lines << "\n";
        cout << "#tabs : " << tabs  << "\n";
        cout << "#words: " << words << "\n";
    }
}; // Count


int main() {
    // initialize the count object
    Count num("austenPandP.txt");

    // process infile one character at a time, incrementing the appropriate
    // count variables
    char c;
    bool last_char_whitespace = true;
    while (num.infile.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    num.print_results();
}
