// count_chars6.cpp

//
// The same as count_chars5.cpp, but ifstream object has been moved into
// Count.
//
// You use it like this:
//
//     $ ./count_chars6
//

#include <iostream>
#include <fstream>

using namespace std;

struct Count {
    ifstream infile = ifstream("austenPandP.txt");

    int chars = 0;
    int lines = 0;
    int tabs  = 0;
    int words = 0;

    void print_results() {
        cout << "#chars: " << chars << "\n";
        cout << "#lines: " << lines << "\n";
        cout << "#tabs : " << tabs  << "\n";
        cout << "#words: " << words << "\n";
    }
}; // Count


int main() {
    // initialize the count object
    Count num;

    // process infile one character at a time, incrementing the appropriate
    // count variables
    char c;
    bool last_char_whitespace = true;
    while (num.infile.get(c)) {
        num.chars++;
        switch (c) {
            case '\n': num.lines++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case '\t': num.tabs++;
                       if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            case ' ' : if (!last_char_whitespace) num.words++;
                       last_char_whitespace = true;
                       break;
            default  : last_char_whitespace = false;
        } // switch
    } // while

    num.print_results();
}
