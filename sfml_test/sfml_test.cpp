// sfml_test.cpp

//
// Draws a size-changing green circle in a window.
//

#include <SFML/Graphics.hpp>


int main() {
    sf::RenderWindow window(sf::VideoMode(200, 200), 
                            "SFML works!");
    window.setFramerateLimit(60);

    sf::CircleShape disk(100.f);
    disk.setFillColor(sf::Color::Green);

    // radiusChange is float because setRadius and getRadius for CircleShape
    // use float
    float radiusChange = -1;

    // main animation loop; or main event loop
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        //
        // update the objects
        //
        if (disk.getRadius() > 100) {
            disk.setFillColor(sf::Color::Green);
            radiusChange = -1;
        } else if (disk.getRadius() < 5) {
            disk.setFillColor(sf::Color::Red);
            radiusChange = 1;
        }

        disk.setRadius(disk.getRadius() + radiusChange);

        // 
        // re-draw the screen
        //
        window.clear();
        window.draw(disk);
        window.display();
    }

    return 0;
}
