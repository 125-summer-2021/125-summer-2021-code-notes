// powers.cpp

#include <iostream>
#include <cassert>

using namespace std;


// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note: 
//    The bases cases can be compressed somewhat. However, we list them
//    individually to make it easier to see that all the cases have been
//    handled.
int power_iter(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    int pow = 1;
    for(int i = 0; i < n; ++i) {
        pow *= a;
    }
    return pow;
}

void power_iter_test() {
    assert(power_iter(2, 0) == 1);
    assert(power_iter(2, 1) == 2);
    assert(power_iter(2, 2) == 2*2);
    assert(power_iter(2, 3) == 2*2*2);
    assert(power_iter(2, 4) == 2*2*2*2);
    assert(power_iter(2, 5) == 2*2*2*2*2);
    cout << "power_iter_test: all tests passed\n";
}


// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
//   recursive
int power_recur(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    return a * power_recur(a, n - 1);
}

// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
///   recursive
int power_recur_fast(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    int half = power_recur_fast(a, n / 2);
    if (n % 2 == 0) {
        return half * half;
    } else {
        return half * half * a;
    }
}

void power_recur_fast_test() {
    assert(power_recur_fast(2, 0) == 1);
    assert(power_recur_fast(2, 1) == 2);
    assert(power_recur_fast(2, 2) == 2*2);
    assert(power_recur_fast(2, 3) == 2*2*2);
    assert(power_recur_fast(2, 4) == 2*2*2*2);
    assert(power_recur_fast(2, 5) == 2*2*2*2*2);
    cout << "power_recur_fast_test: all tests passed\n";
}

int mult_count = 0;

// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
//   uses global variable mult_count to count multiplications
int power_recur_fast2(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    if (n % 2 == 0) {
        int half = power_recur_fast2(a, n / 2);
        mult_count += 1;
        return half * half;
    } else {
        int half = power_recur_fast2(a, (n - 1) / 2);
        mult_count += 2;
        return a * half * half;
    }
}

void power_print2(int a, int n) {
    mult_count = 0;
    power_recur_fast2(a, n);
    cout << a << "^" << n << ", " << mult_count 
	     << " multiplications" << "\n";
}

void power_test2() {
    cout << "\n";
    for(int i = 0; i <= 20; ++i) {
        power_print2(2, i);
    }
}

// 2^0, 0 multiplications
// 2^1, 2 multiplications
// 2^2, 3 multiplications
// 2^3, 4 multiplications
// 2^4, 4 multiplications
// 2^5, 5 multiplications
// 2^6, 5 multiplications
// 2^7, 6 multiplications
// 2^8, 5 multiplications
// 2^9, 6 multiplications
// 2^10, 6 multiplications
// 2^11, 7 multiplications
// 2^12, 6 multiplications
// 2^13, 7 multiplications
// 2^14, 7 multiplications
// 2^15, 8 multiplications
// 2^16, 6 multiplications
// 2^17, 7 multiplications
// 2^18, 7 multiplications
// 2^19, 8 multiplications
// 2^20, 7 multiplications

// Pre-condition: 
//    n >= 0
// Post-condition: 
//    returns a to the power of n, i.e. a^n
// Note:
//    uses global variable mult_count to count multiplications,
//    prints a different character depending on whether the if
//    or else part is executed 
int power_recur_fast3(int a, int n) {
    assert(n >= 0);

    if (a == 0 && n == 0) return 1;
    if (a == 0 && n != 0) return 0;
    if (a != 0 && n == 0) return 1;
    if (a == 1) return 1;

    if (n % 2 == 0) {
        int half = power_recur_fast3(a, n / 2);
        mult_count += 1;
        cout << 0;           // print 1
        return half * half;
    } else {
        int half = power_recur_fast3(a, (n - 1) / 2);
        mult_count += 2;
        cout << 1;          // print 2
        return a * half * half;
    }
}

void power_print3(int a, int n) {
    mult_count = 0;
    power_recur_fast3(a, n);
	cout << " " << a << "^" << n << ", " 
         << mult_count << " multiplications\n";
}

void power_test3() {
    cout << "\n";
    for(int i = 0; i <= 20; ++i) {
        power_print3(2, i);
    }
}

// 2
// 21
// 22
// 211
// 212
// 221
// 222
// 2111
// 2112
// 2121
// 2122
// 2211
// 2212
// 2221
// 2222
// 21111
// 21112
// 21121
// 21122
// 21211

int main() {
    // power_iter_test();
    // power_recur_fast_test();
    // power_test2();
    power_test3();
}
