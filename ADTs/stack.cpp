// stack.cpp

#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

//
// Stack_base is an abstract base class (ABC), i.e. a class where some of the
// methods are not implemented (i.e. they are abstract).
//
// virtual at the start of a methods means that the method can be over-ridden
// in a subclass (i.e. a class that inherits from Stack_base). 
//
// = 0 at the end of a method means there is no implementation of the method
// in this class, and so an implementation must be provided by a subclass.
//
class Stack_base {
public:
    virtual ~Stack_base() {}  // always include a virtual destructor in a base
                              // class; this allows inheriting classes to define
                              // their own destructor

    // Pre-condition:
    //    none
    // Post-condition:
    //    returns true if the stack is empty, and false otherwise
    virtual bool is_empty() const = 0;

    // Pre-condition:
    //    none
    // Post-condition:
    //    puts x on the top of the stack
    virtual void push(double x) = 0;

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   removes and returns the top element of the stack
    virtual double pop() = 0;

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   returns a copy of the top element of the stack (without removing it)
    virtual double peek() const = 0;

    // Pre-condition:
    //    none
    // Post-condition: 
    //   pops all elements from the stack
    // Note:
    //   pop_all() is virtual, which means a subclass can override it.
    //   But it is not abstract, i.e. it has a default implementation, and
    //   so a subclass may choose not to provide its own implementation.
    virtual void pop_all() {
        while (!is_empty()) pop();
    }

}; // class Stack_base

class Vec_stack : public Stack_base {
private:
    vector<double> v;
public:
    // default constructor makes an empty stack
    Vec_stack() { }

    // Pre-condition:
    //    none
    // Post-condition:
    //    returns true if the stack is empty, and false otherwise
    bool is_empty() const { 
        return v.size() == 0; 
    }

    // Pre-condition:
    //    none
    // Post-condition:
    //    puts x on the top of the stack
    void push(double x) {
        v.push_back(x);
    }

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   removes and returns the top element of the stack
    double pop() {
        double result = peek();
        v.pop_back();
        return result;
    }

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   returns a copy of the top element of the stack (without removing it)
    double peek() const {
        return v.back();
    }
}; // class Vec_stack

void vec_stack_test() {
    Vec_stack s;
    assert(s.is_empty());

    s.push(5.4);
    assert(!s.is_empty());
    assert(s.peek() == 5.4);
    
    double top = s.pop();
    assert(top == 5.4);
    assert(s.is_empty());

    s.push(2);
    s.push(3);
    s.push(4);
    assert(!s.is_empty());
    assert(s.peek() == 4);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 3);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 2);

    s.pop();
    assert(s.is_empty());

    s.push(5);
    s.push(6);
    s.pop_all();
    assert(s.is_empty());

    cout << "All Vec_stack tests passed\n";
}

//
// A linked list implementation of a stack.
// 
class List_stack : public Stack_base {
private:
    class Node {
    public:
        double val;
        Node* next;
    };

    Node* head = nullptr;
public:
    // default constructor makes an empty stack
    List_stack()  { }

    // destructor: delete all nodes on the list
    ~List_stack() {
        pop_all();
    }

    // Pre-condition:
    //    none
    // Post-condition:
    //    returns true if the stack is empty, and false otherwise
    bool is_empty() const { 
        return head == nullptr; 
    }

    // Pre-condition:
    //    none
    // Post-condition:
    //    puts x on the top of the stack
    void push(double x) {
        head = new Node{x, head};
    }

    // Pre-condition:
    //    !is_empty()
    // Post-condition: 
    //   removes and returns the top element of the stack
    double pop() {
        double result = head->val;
        Node* p = head;
        head = head->next;
        delete p;
        return result;
    }

    // Pre-condition:
    //   !is_empty()
    // Post-condition: 
    //   returns a copy of the top element of the stack 
    //   (without removing it)
    double peek() const {
        return head->val;
    }

}; // class List_stack

void list_stack_test() {
    List_stack s;
    assert(s.is_empty());

    s.push(5.4);
    assert(!s.is_empty());
    assert(s.peek() == 5.4);
    
    double top = s.pop();
    assert(top == 5.4);
    assert(s.is_empty());

    s.push(2);
    s.push(3);
    s.push(4);
    assert(!s.is_empty());
    assert(s.peek() == 4);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 3);

    s.pop();
    assert(!s.is_empty());
    assert(s.peek() == 2);

    s.pop();
    assert(s.is_empty());

    s.push(5);
    s.push(6);
    s.pop_all();
    assert(s.is_empty());

    cout << "All List_stack tests passed\n";
}

// Returns the number of elements in stack s. It does this in a rather
// inefficient way, since Stack_base doesn't provide a size() method.
//
// Note that we cannot pass s by constant reference because size must call
// s.pop(), i.e. size must modify s.
//
// Importantly, the type of s is Stack_base&, and so s could be either
// Vec_stack or List_stack. The code inside size only uses methods listed in
// Stack_base.
int size(Stack_base& s) {
    int result = 0;

    // pop each element from s and store them in temp
    Vec_stack temp;
    while (!s.is_empty()) {
        temp.push(s.pop());
        result++;
    }

    // push all items back into s
    while (!temp.is_empty()) {
        s.push(temp.pop());
    }

    return result;
}

void size_test() {
    Vec_stack vs;
    List_stack ls;
    assert(size(vs) == 0);
    assert(size(ls) == 0);

    vs.push(6);
    ls.push(6);
    assert(size(vs) == 1);
    assert(size(ls) == 1);    

    vs.push(7);
    ls.push(7);
    assert(size(vs) == 2);
    assert(size(ls) == 2);  

    vs.pop();
    ls.pop();
    assert(size(vs) == 1);
    assert(size(ls) == 1);

    cout << "All size tests passed\n";
}

int main() {
    vec_stack_test();
    list_stack_test();
    size_test();
}
