// linked_list3.cpp

//
// Based on linked_list2.cpp, but puts head, the Node struct, and the
// functions in a class. In addition, it includes the method is_empty() that
// returns true if the list is empty (i.e. head == nullptr), and false
// otherwise.
//

#include "cmpt_error.h"
#include <iostream>

using namespace std;

class List {
	struct Node {    // The user of List does not need to know about
		int   data;  // the existence of the Node struct, so we make it
		Node* next;  // private inside List.
	};

	Node* head;

public:
	// default constructor
	List()
	: head(nullptr)   // initialization list
	{ }

	// destructor: deletes all elements of the list
	~List() {
		while (!is_empty()) {
			remove_front();
		}
	}

	bool is_empty() const { 
		return head == nullptr;
	}

	void add_front(int data) {
		head = new Node{data, head};
	}

	int remove_front() {
		if (is_empty()) cmpt::error("remove_front: empty list");
		int result = head->data;
		Node* temp = head->next;
		delete head;
		head = temp;
		return result;
	}

	void print() const {
		Node* p = head;
		while (p != nullptr) {
			cout << p->data << "\n";
			p = p->next;
		}
	}
}; // class List

int main() {
    // add the numbers 1, 2, 3 to the list
    List a;
    a.add_front(1);
    a.add_front(2);
    a.add_front(3);

	// print the values of the list
	a.print();

	// // remove the elements one at a time
	// a.remove_front();
	// a.remove_front();
	// a.remove_front();
}
