// linked_list5.cpp

//
// Based on linked_list4.cpp, but all the methods that used loops now use
// recursion instead of loops. In each case a private helper method was
// created that takes one extra pointer as a parameter. The corresponding
// public method calls the private helper method.
//

#include "cmpt_error.h"
#include <iostream>
#include <cassert>

using namespace std;

class List {
	struct Node {
		int data;
		Node* next;
	};

	Node* head;

	// returns the number of nodes from p to the end of the list
	int size(Node* p) const {
		if (p == nullptr) {
			return 0;
		} else {
			return 1 + size(p->next);
		}
	}

	// prints the number of nodes from p to the end of the list
	void print(Node* p) const {
		if (p != nullptr) {
			cout << p->data << "\n";
			print(p->next);
		}
	}

	// returns the sum of node values from p to the end of the list
	int sum(Node* p) const {
		if (p == nullptr) {
			return 0;
		} else {
			return p->data + sum(p->next);
		}
	}

	// returns the value on the last node of the list that p points to
	int last(Node* p) const {
		if (p == nullptr) cmpt::error("last: nullptr");
		if (p->next == nullptr) {
			return p->data;
		} else {
			return last(p->next);
		}
	}

    // returns true if a node on the list p points to contains n as a value,
    // and false otherwise
	bool contains(Node* p, int n) const {
		if (p == nullptr) {
			return false;
		} else if (p->data == n) {
			return true;
		} else {
			return contains(p->next, n);
		}
	}

public:
	// default constructor
	List()
	: head(nullptr)   // initialization list
	{ }

	// destructor: deletes all elements of the list
	~List() {
		while (!is_empty()) {
			// cout << "~List: removing " << remove_front() << "\n";
			remove_front();
		}
	}

	bool is_empty() const { 
		return head == nullptr;
	}

	int first() const { 
		if (is_empty()) cmpt::error("first: empty list");
		return head->data;
	}

	void add_front(int data) {
		head = new Node{data, head};
	}

	int remove_front() {
		if (is_empty()) cmpt::error("remove_front: empty list");
		int result = head->data;
		Node* temp = head->next;
		delete head;
		head = temp;
		return result;
	}

	void print() const {
		print(head);
	}

	int size() const {
		return size(head);
	}

	int sum() const {
		return sum(head);
	}

	bool contains(int n) const {
		return contains(head, n);
	}

	int last() const {
		if (is_empty()) cmpt::error("last: empty list");
		return last(head);
	}

	friend bool operator==(const List& a, const List& b);
}; // class List

// Note: It would cause a memory error if a and b were passed by value here
// becauses the head pointers to the start of the underlying lists would be
// copied, but none of the nodes would be copied. The nodes deleted by the
// destructor would be the same nodes as the passed-in nodes, resulting a
// double-deletion memory error.
bool operator==(const List& a, const List& b) {
	if (a.size() != b.size()) return false;
	assert(a.size() == b.size());
	List::Node* ap = a.head;
	List::Node* bp = b.head;
	while (ap != nullptr) {
		assert(bp != nullptr);
		if (ap->data != bp->data) return false;
		ap = ap->next;
		bp = bp->next;
	}
	return true;
}

bool operator!=(const List& a, const List& b) {
	return !(a == b);
}

int main() {
    // add the numbers 1, 2, 3 to the list
    List a;
    assert(a.size() == 0);
    assert(a.sum() == 0);
    assert(!a.contains(5));
    assert(a == a);

    a.add_front(1);
    assert(a.size() == 1);
    assert(a.first() == 1);
    assert(a.sum() == 1);
    assert(a.contains(1));
    assert(!a.contains(5));
    assert(a.last() == 1);
    assert(a == a);

    a.add_front(2);
    assert(a.size() == 2);
    assert(a.first() == 2);
    assert(a.sum() == 3);
    assert(a.contains(1));
    assert(a.contains(2));
    assert(!a.contains(5));
    assert(a.last() == 1);
    assert(a == a);

    a.add_front(3);
    assert(a.size() == 3);
    assert(a.first() == 3);
    cout << "a.sum: " << a.sum() << "\n";
    a.print();
    assert(a.sum() == 6);
    assert(a.contains(1));
    assert(a.contains(2));
    assert(a.contains(3));
    assert(!a.contains(5));
    assert(a.last() == 1);
    assert(a == a);

	// print the values of the list
	a.print();
	cout << "      First: " << a.first()     << "\n";
	cout << "       Size: " << a.size()      << "\n";
	cout << "        Sum: " << a.sum()       << "\n";
	cout << " contains 3: " << a.contains(3) << "\n";
	cout << " contains 4: " << a.contains(4) << "\n";
	cout << "       Last: " << a.last()      << "\n";
	cout << "     a == a: " << (a == a)      << "\n";

	List b;
	assert(a != b);
	cout << "     a == b: " << (a == b) << "\n";
    
    b.add_front(1);
    assert(a != b);
    cout << "     a == b: " << (a == b) << "\n";

    b.add_front(2);
    assert(a != b);
    cout << "     a == b: " << (a == b) << "\n";
    b.add_front(3);
    assert(a == b);
    cout << "     a == b: " << (a == b) << "\n";
}
