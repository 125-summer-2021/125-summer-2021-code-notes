// linked_list1.cpp

//
// Creates a singly-linked lists of ints and adds 3 numbers and then removes 3
// numbers.
//

#include <iostream>

using namespace std;

struct Node {
	int   data;
	Node* next;
};

int main() {
    // add the numbers 1, 2, 3 to the list
	Node* head = nullptr;
	head = new Node{1, head};
	head = new Node{2, head};
	head = new Node{3, head};

	// print the values of the list
	cout << head->data             << "\n"; // 3
	cout << head->next->data       << "\n"; // 2
	cout << head->next->next->data << "\n"; // 1

	// remove the elements one at a time
	Node* temp;

	temp = head->next;
	delete head;
	head = temp;

	temp = head->next;
	delete head;
	head = temp;

	temp = head->next;
	delete head;
	head = temp;
}
