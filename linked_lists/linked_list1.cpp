// linked_list1.cpp

//
// Creates a singly-linked lists of ints and adds 3 numbers and then removes 3
// numbers.
//

#include <iostream>

using namespace std;

struct Node {
	int   data;
	Node* next;
};

void add_front(Node*& h, int value) {
	h = new Node{value, h};
}

int main() {
	//
	// initialize the head pointer
	//
	Node* head = nullptr;

	//
    // add the numbers 1, 2, 3 to the start of list
	//
	add_front(head, 1);
	add_front(head, 2);
	add_front(head, 3);
	// head = new Node{1, head};
	// head = new Node{2, head};
	// head = new Node{3, head};

	//
	// print the values of the list
	//
	cout << head->data             << "\n"; // 3
	cout << head->next->data       << "\n"; // 2
	cout << head->next->next->data << "\n"; // 1

 //    //
 //    // print the values of the list using a pointer
 //    //
 //    cout << "\n";

 //    Node* p = head;
   
 //    cout << p->data << "\n";
 //    p = p->next;

 //    cout << p->data << "\n";
 //    p = p->next;
    
 //    cout << p->data << "\n";
 //    p = p->next;  
 //    // p == nullptr

 //    //
 //    // print the values of the list using a loop
 //    //
 //    cout << "\n";
 //    p = head;
 //    while (p != nullptr) {
	//     cout << p->data << "\n";
	//     p = p->next; 
 //    }

	// //
	// // remove the elements one at a time
	// //
	// Node* temp;

	// temp = head->next;
	// delete head;
	// head = temp;

	// temp = head->next;
	// delete head;
	// head = temp;

	// temp = head->next;
	// delete head;
	// head = temp;
}
