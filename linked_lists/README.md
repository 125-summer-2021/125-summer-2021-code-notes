## Linked Lists

- [linked_list1.cpp](linked_list1.cpp) Shows the basic idea of a linked list,
  adding 3 numbers to the start and then removing them.

- [linked_list2.cpp](linked_list2.cpp) Based on
  [linked_list1.cpp](linked_list1.cpp), but uses functions for adding an
  element, removing the first element, and printing items on the list.

- [linked_list3.cpp](linked_list3.cpp) Based on
  [linked_list2.cpp](linked_list2.cpp), but puts `head`, the `Node` struct,
  and the functions in a class. In addition, it includes the method
  `is_empty()` that returns `true` if the list is empty (i.e. `head ==
  nullptr`), and `false` otherwise.

- [linked_list4.cpp](linked_list4.cpp) Based on
  [linked_list3.cpp](linked_list3.cpp), adds a few more convenient methods:

  - `first()`, returns the first number in the list

  - `size()`, returns the number of elements in the list

  - `sum()`, returns the sum of the numbers in the list

  - `contains(n)`, returns `true` if `n` appears in the list, and `false`
    otherwise

  - `last()`, returns the last number in the list

  - `operator==(a,b)`, a function (not a method!) that returns `true` when the
    lists `a` and `b` have the same elements in the same order, and `false`
    otherwise

  - `operator!=(a,b)`, returns the negation of `operator==(a,b)`


- [linked_list5.cpp](linked_list5.cpp) Based on
  [linked_list4.cpp](linked_list4.cpp), but all the methods that used loops
  now use **recursion** instead. In each case a private helper method was
  created that takes one extra pointer as a parameter. The corresponding
  public method calls the private helper method.

  It also includes some `assert`-based tests to check for correctness.
