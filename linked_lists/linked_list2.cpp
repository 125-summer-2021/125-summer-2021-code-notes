// linked_list2.cpp

//
// Based on linked_list1.cpp, but uses functions for adding elements, removing
// the first element, and printing items on the list.
//

#include <iostream>
#include "cmpt_error.h"

using namespace std;

struct Node {
	int   data;
	Node* next;
};

// Notice that head is a pointer that is passed by reference.
void add_front(Node*& head, int data) {
	head = new Node{data, head};
}

// error: can't declare pointer to a Node&
// void insert_front(Node&* h, int data) {
//     h = new Node{data, h};
// }

int remove_front(Node*& head) {
	if (head == nullptr) cmpt::error("remove_front: empty list");
	int result = head->data;
	Node* temp = head->next;
	delete head;
	head = temp;
	return result;
}

void print(Node* p) {
	while (p != nullptr) {
		cout << p->data << "\n";
		p = p->next;
	}
}

int main() {
    // add the numbers 1, 2, 3 to the list
	Node* head = nullptr;
	add_front(head, 1);
	add_front(head, 2);
	add_front(head, 3);

	// print the values of the list
	print(head);

	// remove the elements one at a time
	remove_front(head);
	remove_front(head);
	remove_front(head);
}
