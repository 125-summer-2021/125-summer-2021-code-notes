// linked_list4.cpp

//
// Based on linked_list3.cpp, adds a few more convenient methods:
//
// - first(), returns the first number in the list
//
// - size(), returns the number of elements in the list
//
// - sum(), returns the sum of the numbers in the list
//
// - contains(n), returns true if n appears in the list, and false otherwise
//
// - last(), returns the last number in the list
//
// - operator==(a,b), a function that returns true when the lists a and b
//   have the same elements in the same order
// 
// - operator!=(a,b), returns the negation of operator==(a,b)
//

#include "cmpt_error.h"
#include <iostream>
#include <cassert>

using namespace std;

class List {
	struct Node {
		int data;
		Node* next;
	};

	Node* head;

public:
	// default constructor
	List()
	: head(nullptr)   // initialization list
	{ }

	// destructor: deletes all elements of the list
	~List() {
		while (!is_empty()) {
			// cout << "~List: removing " << remove_front() << "\n";
			remove_front();
		}
	}

	bool is_empty() const { 
		return head == nullptr;
	}

	int first() const { 
		if (is_empty()) cmpt::error("first: empty list");
		return head->data;
	}

	void add_front(int data) {
		head = new Node{data, head};
	}

	int remove_front() {
		if (is_empty()) cmpt::error("remove_front: empty list");
		int result = head->data;
		Node* temp = head->next;
		delete head;
		head = temp;
		return result;
	}

	void print() const {
		Node* p = head;
		while (p != nullptr) {
			cout << p->data << "\n";
			p = p->next;
		}
	}

	int size() const {
		int result = 0;
		Node* p = head;
		while (p != nullptr) {
			result++;
			p = p->next;
		}
		return result;
	}

	int sum() const {
		int result = 0;
		Node* p = head;
		while (p != nullptr) {
			result += p->data;
			p = p->next;
		}
		return result;
	}

	bool contains(int n) const {
		Node* p = head;
		while (p != nullptr) {
			if (p->data == n) return true;
			p = p->next;
		}
		return false;
	}

	int last() const {
		if (is_empty()) cmpt::error("last: empty list");
		Node* p = head;
		while (p->next != nullptr) {
			p = p->next;		
		}
		return p->data;
	}

	friend bool operator==(const List& a, const List& b);
}; // class List

// Note: It would cause a memory error if a and b were passed by value here
// becauses the head pointers to the start of the underlying lists would be
// copied, but none of the nodes would be copied. The nodes deleted by the
// destructor would be the same nodes as the passed-in nodes, resulting a
// double-deletion memory error.
bool operator==(const List& a, const List& b) {
	if (a.size() != b.size()) return false;
	assert(a.size() == b.size());
	List::Node* ap = a.head;
	List::Node* bp = b.head;
	while (ap != nullptr) {
		assert(bp != nullptr);
		if (ap->data != bp->data) return false;
		ap = ap->next;
		bp = bp->next;
	}
	return true;
}

bool operator!=(const List& a, const List& b) {
	return !(a == b);
}

int main() {
    // add the numbers 1, 2, 3 to the list
    List a;
    a.add_front(1);
    a.add_front(2);
    a.add_front(3);

	// print the values of the list
	a.print();
	cout << "      First: " << a.first()     << "\n";
	cout << "       Size: " << a.size()      << "\n";
	cout << "        Sum: " << a.sum()       << "\n";
	cout << " contains 3: " << a.contains(3) << "\n";
	cout << " contains 4: " << a.contains(4) << "\n";
	cout << "       Last: " << a.last()      << "\n";
	cout << "     a == a: " << (a == a)      << "\n";

	List b;
	cout << "     a == b: " << (a == b) << "\n";
    b.add_front(1);
    cout << "     a == b: " << (a == b) << "\n";
    b.add_front(2);
    cout << "     a == b: " << (a == b) << "\n";
    b.add_front(3);
    cout << "     a == b: " << (a == b) << "\n";
}
