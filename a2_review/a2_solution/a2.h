// a2.h

// You can #include any standard C++ files that you need. Do *not* include any
// non-standard C++ files.

#include "int_list.h"  // keep this: contains the int_list struct
#include "cmpt_error.h"
#include <string>
#include <cassert>
#include <iostream>
#include <fstream>

using namespace std;

//
// Checks if the state of an int_list is valid.
//
void check_rep(int_list arr) {
    assert(0 <= arr.size);
    assert(arr.size <= arr.capacity);
    assert(arr.capacity >= 0);
}

//
// Returns a new int_list of size 0.
//
int_list make_new(int cap = 5) {
    int_list result = int_list{new int[cap], // data
                               0,            // size
                               cap           // capacity
                              };
    check_rep(result);
    return result;
}

//
// The programmer must remember to call deallocate exactly once on any
// int_list they no longer want. Forgetting to call it results in a memory
// leak, and calling it more than once will most likely corrupt memory!
//
void deallocate(int_list& arr) {
    delete[] arr.data;
    arr.data = nullptr;
}

//
// Returns a new int_list that is a copy of v. The returned int_list does
// *not* share the same underlying array as v.
//
int_list copy(const int_list& other) {
    // Step 1: make the new int list and underlying array
    int_list result = int_list{new int[other.capacity], 
                               other.size, 
                               other.capacity};
    // Step 2: copy the array values from other.data into result.data
    for(int i = 0; i < other.size; ++i) {
        result.data[i] = other.data[i];
    }
    check_rep(result);
    return result;
}

//
// Returns true iff a and b are equal.
//
bool operator==(int_list a, int_list b) {
    if (a.size != b.size) return false;
    for(int i = 0; i < a.size; ++i) {
        if (a.data[i] != b.data[i]) {
            return false;
        }
    }
    return true;
}

//
// Returns true iff a and b are not equal.
//
bool operator!=(int_list a, int_list b) {
    return !(a == b);
}

//
// Removes all elements of arr (by setting its size to 0).
//
void clear(int_list& arr) {
    arr.size = 0;
    check_rep(arr);
} 

//
// Returns the percentage of the array that is in use.
//
double pct_used(const int_list& arr) {
    return double(arr.size) / arr.capacity;
}

//
// Returns a string containing all the values of arr, separated by sep.
//
string join(const int_list& arr, const string& sep) {
    if (arr.size == 0) {
        return "";
    } else {
        string result;
        result += to_string(arr.data[0]);
        for(int i = 1; i < arr.size; ++i) {  // i starts at 1 (not 0)
            result += sep + to_string(arr.data[i]);
        }
        return result;
    }
}

//
// Returns a nicely-formatted string representation of arr.
//
string to_str(const int_list& arr) {
    return "{" + join(arr, ", ") + "}";
}

//
// Write arr to the console in a nice format.
//
void write(const int_list& arr) {
    cout << to_str(arr);
}

//
// Write arr to the console, followed by a newline.
//
void writeln(const int_list& arr) {
    write(arr);
    cout << '\n';
}

//
// Helper function for debugging.
//
void write_info(const int_list& arr) {
    cout << "<size=" << arr.size << ", cap=" << arr.capacity << ", " 
         << "used=" << pct_used(arr) << ", " << to_str(arr) << ">\n";
}

//
// Helper function for checking array bounds.
//
void check_range(const int_list& arr, int i, const string& msg) {
    if (i < 0 || i >= arr.size) cmpt::error(msg);
}

//
// Returns a copy of the int at index location i.
//
int get(const int_list& arr, int i) {
    check_range(arr, i, "get: index out of range");
    return arr.data[i];
}

//
// Sets index location i to be the value of x.
//
void set(int_list& arr, int i, const int& x) {
    check_range(arr, i, "set: index out of range");
    arr.data[i] = x;
    check_rep(arr);
}

void enlarge(int_list& arr, int incr) {
    assert(incr >= 0);
    // Step 1: calculate the new capacity
    int new_cap = incr + arr.capacity;

    // Step 2: make a new array
    int* new_arr = new int[new_cap];

    // Step 3: copy arr.data values into new_arr
    for(int i = 0; i < arr.size; i++) {
        new_arr[i] = arr.data[i];
    }

    // Step 4: de-allocate the old underlying array
    delete[] arr.data;

    // Step 5: make arr.data point to the new array, and set its capacity
    arr.data = new_arr;
    arr.capacity = new_cap; 
}

//
// append_right(arr, x) adds x to the right end of arr. If arr doesn't have
// enough space to hold x, then its underlying array is doubled.
//
void append_right(int_list& arr, const int& x) { // note that arr is not const 
    // Step 1: check if re-sizing is needed
    if (arr.size >= arr.capacity) {
        enlarge(arr, arr.capacity);
    }
    assert(arr.size < arr.capacity);

    // Step 2: add x to the end of the array, and increment the size
    arr.data[arr.size] = x;
    arr.size++;
    check_rep(arr);
}

// Add x to the start of arr, re-sizing if necessary.
void append_left(int_list& arr, const int& x) { // note that arr is not const 
    // Step 1: check if re-sizing is needed
    if (arr.size >= arr.capacity) {
        enlarge(arr, arr.capacity);
    }

    // Step 2: Move all current elements up one position. Moving starts at the
    // right end to avoid over-writing.
    for(int i = arr.size; i > 0; i--) {
        arr.data[i] = arr.data[i-1];
    }

    // Step 3: add x to the start of the array, and increment the size
    arr.data[0] = x;
    arr.size++;
    check_rep(arr);
}

//
// If the capacity of arr is bigger than its size, then re-size the underlying
// array.
//
void shrink_to_fit(int_list& arr) {
    // Step 1: check if re-sizing is necessary
    if (arr.size == arr.capacity) return; // do nothing if already min size

    assert(arr.size < arr.capacity);

    // Step 2: calculate the new capacity
    int new_cap = arr.size;
    
    // Step 3: make a new array of size new_cap
    int* new_arr = new int[new_cap];
    
    // Step 4: copy arr.data into new_arr
    for(int i = 0; i < arr.size; ++i) {
        new_arr[i] = arr.data[i];
    }

    // Step 5: de-allocate the old underlying array
    delete[] arr.data;
    
    // Step 6: make arr.data point to the new array, and set the new capacity
    // (arr.size is unchanged)
    arr.data = new_arr;
    arr.capacity = new_cap;
    
    check_rep(arr);
}

//
// Returns a new int_list containing all the numbers in fname.
//
int_list make_fromFile(const string& fname) {
    // Step 1: open the file
    ifstream fin(fname);
    
    // Step 2: create a new empty int_list
    int_list result = make_new();
    
    // Step 3: read the ints one at a time from the file, and append them to
    // the int_list
    int n;
    while (fin >> n) {
        append_right(result, n);
    } 
    return result;
}

//
// Returns the sum the numbers in arr.
//
int sum(const int_list& arr) {
    // Step 1: initialize the sum to 0
    int result = 0;

    // Step 2: add each int in arr to the sum
    for(int i = 0; i < arr.size; i++) {
        result += arr.data[i];
    }
    return result;
}

//
// Returns the average the numbers in arr.
//
double average(const int_list& arr) {
    if (arr.size == 0) {
        return 0;
    }
    return sum(arr) / double(arr.size);
}

//
// Returns a new int_list consisting of the elements of a followed by the
// elements of b.
//
int_list concat(int_list a, int_list b) {
    int_list result = copy(a);
    for(int i = 0; i < b.size; i++) {
        append_right(result, b.data[i]);
    } 

    return result;
}
