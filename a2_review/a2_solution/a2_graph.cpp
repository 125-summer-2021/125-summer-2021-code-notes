// a2_graph.cpp

#include "a2.h"
#include <SFML/Graphics.hpp>
#include <vector>

using namespace std;

// screen dimensions
const int WIDTH  = 500;
const int HEIGHT = 500;

// graph parameters
const int MIN_NUMS = 10;
const int MAX_NUMS = 100;

int main(int argc, char* argv[])
{
    //
    // check that the user passes in the name of a file
    //
    if (argc != 2) {
        cout << "error: pass in name of data file\n";
        return -1;
    }

    string fname(argv[1]);
    cout << "reading " << fname << "\n";

    //
    // get the data
    //
    int_list nums = make_fromFile(fname);
    write_info(nums);

    //
    // check that there from 1 to MAX_NUMS numbers
    //
    if (nums.size < MIN_NUMS || nums.size > MAX_NUMS) {
        cout << "error: data.txt must have 1 to " << MAX_NUMS 
             << " non-negative integers\n";
        return -1;
    }

    //
    // check that each number is on the range 0 to 499
    //
    for (int i = 0; i < nums.size; i++) {
        if (get(nums, i) < 0 || get(nums, i) > 499) {
            cout << "error: data.txt integers must be in range 0 to 499\n";
            return -1;
        }
    }

    //
    // create the graphics window
    //
    sf::RenderWindow win(sf::VideoMode(WIDTH, HEIGHT), "Assignment 2 Bar Chart");
    win.setFramerateLimit(60);

    //
    // create one rectangle for each number
    //
    const float SPACE     = WIDTH / nums.size;
    const float BAR_WIDTH = SPACE / 2;
    const float BAR_SEP   = SPACE / 2;
    const float START = BAR_SEP;
    cout << "    SPACE: " << SPACE     << "\n";
    cout << "BAR_WIDTH: " << BAR_WIDTH << "\n";
    cout << "  BAR_SEP: " << BAR_SEP   << "\n";
    cout << "    START: " << START     << "\n";

    vector<sf::RectangleShape> bars(nums.size);
    for (int i = 0; i < bars.size(); i++) {
        const float height = float(get(nums, i));
        bars[i].setSize({BAR_WIDTH, height});
        bars[i].setPosition(i*(BAR_WIDTH + BAR_SEP) + START, 499);
        bars[i].setOrigin(0, height);
    }

    //
    // main animation loop
    //
    while (win.isOpen())
    {
        // check all the window's events that were triggered since the last
        // iteration of the loop
        sf::Event event;
        while (win.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                win.close();
            } 
        } // while win.PollEvent

        // clear the screen
        win.clear(sf::Color::Black);

        //
        // draw the rectangles here
        //
        for (sf::RectangleShape& r : bars) {
            win.draw(r);
        }

        //
        // end the current frame and display the window
        //
        win.display();
    } // while

    deallocate(nums);

    return 0;
} // main
