// a2_test.cpp

// You can #include other standard C++ files. But do *not* include any
// non-standard C++ files.

#include "a2.h"
#include "cmpt_error.h"
#include <iostream>
#include <algorithm>

using namespace std;

void test_make_new() {
    cout << "Running test_make_new ... ";
    int_list a = make_new();
    assert(a.size == 0);
    deallocate(a);

    int_list b = make_new(100);
    assert(b.size == 0);
    deallocate(b);

    cout << "all test_make_new tests passed\n";
}

void test_deallocate() {
    cout << "Running test_deallocate ... ";
    int_list a = make_new();

    int_list b = make_new(100);
    for(int i = 0; i < 1000; i++) append_right(b, i);
    deallocate(b);
    assert(b.data == nullptr);

    int_list c = copy(a); 
    deallocate(c);
    assert(c.data == nullptr);

    deallocate(a);
    assert(a.data == nullptr);
    cout << "all test_deallocate tests passed\n";
}

void test_copy() {
    cout << "Running test_copy ... ";
    int_list a = make_new();
    int_list b = copy(a);
    assert(a.size == b.size);
    assert(a == b);
    for(int i = 0; i < 1000; i++) append_left(a, i);
    assert(a.size != b.size);
    int_list c = copy(a);
    assert(a.size == c.size);
    assert(a == c);
    for(int i = 0; i < 1000; i++) {
       assert(get(a, i) == get(c, i));
    }
    deallocate(a);
    deallocate(b);
    deallocate(c);
    cout << "all test_copy tests passed\n";
}

void test_to_str() {
    cout << "Running test_to_str ... ";
    int_list a = make_new();
    assert(to_str(a) == "{}");
    append_right(a, 1);
    assert(to_str(a) == "{1}");
    append_left(a, 2);
    assert(to_str(a) == "{2, 1}");
    append_left(a, 3);
    assert(to_str(a) == "{3, 2, 1}");
    deallocate(a);
    cout << "all test_to_str tests passed\n";
}

void test_get() {
    cout << "Running test_get ... ";
    int_list a = make_new();
    for(int i = 0; i < 100; i++) {
        append_right(a, i);
        assert(get(a, i) == i);  
    }
    deallocate(a);
    cout << "all test_get tests passed\n";
}

void test_set() {
    cout << "Running test_set ... ";
    int_list a = make_new();
    for(int i = 0; i < 100; i++) {
        append_right(a, 0);
    }
    set(a, 0, 5);
    assert(get(a, 0) == 5);
    set(a, 0, 6);
    assert(get(a, 0) == 6);
    set(a, a.size-1, 9);
    assert(get(a, a.size-1) == 9);
    deallocate(a);
    cout << "all test_set tests passed\n";
}

void test_append_right() {
    cout << "Running test_append_right ... ";
    int_list a = make_new();
    assert(a.size == 0);
    append_right(a, 2);
    assert(a.size == 1);
    assert(get(a, 0) == 2);
    append_right(a, 3);
    assert(a.size == 2);
    assert(get(a, 0) == 2);
    assert(get(a, 1) == 3);
    for(int i = 0; i < 1000; i++) append_right(a, i);
    assert(a.size == 1002);
    assert(get(a, 0) == 2);
    assert(get(a, 1) == 3);
    assert(get(a, 2) == 0);
    assert(get(a, a.size-1) == 999);

    deallocate(a);
    cout << "all test_append_right tests passed\n";
}

void test_equal() {
    cout << "Running test_equal ... ";
    int_list a = make_new();
    assert(a == a);
    int_list b = make_new();
    assert(a == b);
    append_right(a, 5);
    append_left(b, 5);
    assert(a == b);
    assert(b == a);
    deallocate(a);
    deallocate(b);
    cout << "all test_equal tests passed\n";
}

void test_not_equal() {
    cout << "Running test_not_equal ... ";
    int_list a = make_new();
    assert(!(a != a));
    int_list b = make_new();
    assert(!(a != b));
    append_right(a, 5);
    assert(a != b);
    assert(b != a);
    append_left(b, 5);
    assert(!(a != b));
    assert(!(b != a));
    deallocate(a);
    deallocate(b);
    cout << "all test_not_equal tests passed\n";
}

void test_clear() {
    cout << "Running test_clear ... ";
    int_list a = make_new();
    clear(a);
    assert(a.size == 0);
    append_right(a, 3);
    assert(a.size == 1);
    clear(a);
    assert(a.size == 0);
    for(int i = 0; i < 1000; i++) append_right(a, i);
    clear(a);
    assert(a.size == 0);
    for(int i = 0; i < 20; i++) append_right(a, i);
    clear(a);
    assert(a.size == 0);
    deallocate(a);
    cout << "all test_clear tests passed\n";
}

void test_append_left() {
    cout << "Running test_append_left ... ";
    int_list a = make_new();
    assert(a.size == 0);
    append_left(a, 2);
    assert(a.size == 1);
    assert(get(a, 0) == 2);
    append_left(a, 3);
    assert(a.size == 2);
    assert(get(a, 0) == 3);
    assert(get(a, 1) == 2);
    for(int i = 0; i < 1000; i++) append_left(a, i);
    assert(a.size == 1002);
    assert(get(a, 0) == 999);
    assert(get(a, 1) == 998);
    assert(get(a, 2) == 997);
    assert(get(a, a.size-1) == 2);

    deallocate(a);
    cout << "all test_append_left tests passed\n";
}

void test_shrink_to_fit() {
    cout << "Running test_shrink_to_fit ... ";
    int_list a = make_new();
    append_right(a, 3);
    assert(a.size != a.capacity);
    shrink_to_fit(a);
    assert(a.size == a.capacity);
    for(int i = 0; i < 1000; i++) append_left(a, i);
    assert(a.size != a.capacity);
    shrink_to_fit(a);
    assert(a.size == a.capacity);
    deallocate(a);
    cout << "all test_shrink_to_fit tests passed\n";
}

// 4 -2 0
// 8000 64 64 -125
void test_make_fromFile() {
    cout << "Running test_make_fromFile ... ";
    int_list a = make_fromFile("test_small.txt");
    assert(a.size == 7);
    assert(get(a, 0) == 4);
    assert(get(a, 1) == -2);
    assert(get(a, 2) == 0);
    assert(get(a, 3) == 8000);
    assert(get(a, 4) == 64);
    assert(get(a, 5) == 64);
    assert(get(a, 6) == -125);

    deallocate(a);
    cout << "all test_make_fromFile tests passed\n";
}

void test_average() {
    cout << "Running test_average ... ";
    int_list a = make_new();
    append_right(a, 2);
    assert(average(a) == (double(2) / 1));
    append_right(a, 5);
    assert(average(a) == (double(2+5) / 2));
    append_right(a, -2);
    assert(average(a) == (double(2+5-2) / 3));
    append_right(a, 0);
    assert(average(a) == (double(2+5-2+0) / 4));
    deallocate(a);
    cout << "all test_average tests passed\n";
}

void test_concat() {
    cout << "Running test_concat ... ";
    int_list a = make_new();
    int_list b = concat(a, a);
    assert(a.size == 0);
    assert(b.size == 0);
    deallocate(a);
    deallocate(b);
    a = make_new();
    b = make_new();
    append_right(a, 2);
    append_right(a, 3);
    append_right(b, 4);
    append_right(b, 5);
    append_right(b, 6);
    int_list c = concat(a, b);
    assert(c.size == 5);
    assert(get(c, 0) == 2);
    assert(get(c, 1) == 3);
    assert(get(c, 2) == 4);
    assert(get(c, 3) == 5);
    assert(get(c, 4) == 6);
    deallocate(a);
    deallocate(b);
    deallocate(c);
    cout << "all test_concat tests passed\n";
}

int main() {
    test_make_new();
    test_deallocate();
    test_copy();
    test_to_str();
    test_get();
    test_set();
    test_append_right();
    test_equal();
    test_not_equal();
    test_clear();
    test_append_left();
    test_shrink_to_fit();
    test_make_fromFile();
    test_average();
    test_concat();
}
