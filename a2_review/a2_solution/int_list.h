// int_list.h

//
// Don't modify this file in any way!!
//
struct int_list {
    int* data;      // pointer to the underlying array
    int  size;      // # of elements from user's perspective
    int  capacity;  // length of underlying array
};
