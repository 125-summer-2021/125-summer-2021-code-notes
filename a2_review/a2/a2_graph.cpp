// a2_graph.cpp

#include "a2.h"
#include <SFML/Graphics.hpp>
#include <iostream>

using namespace std;

// screen dimensions
const int WIDTH  = 500;
const int HEIGHT = 500;

// graph parameters
const int MIN_NUMS = 10;
const int MAX_NUMS = 100;

int main(int argc, char* argv[])
{
    //
    // check that the user passes in the name of a file
    //
    if (argc != 2) {
    	cout << "Error: pass in the name of the data file, e.g.:\n";
        cout << "   ./a2_graph data.txt\n";
        return -1;
    }

    string fname(argv[1]);
    cout << "reading " << fname << "\n";

    //
    // get the data
    //

    //
    // check that there from 1 to MAX_NUMS numbers
    //

    //
    // check that each number is on the range 0 to 499
    //

    //
    // create the graphics window
    //
    sf::RenderWindow win(sf::VideoMode(WIDTH, HEIGHT), "Assignment 2 Bar Chart");
    win.setFramerateLimit(60);

    //
    // create one rectangle for each number
    //


    //
    // main animation loop
    //
    while (win.isOpen())
    {
        // check all the window's events that were triggered since the last
        // iteration of the loop
        sf::Event event;
        while (win.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                win.close();
            } 
        } // while win.PollEvent

        // clear the screen
        win.clear(sf::Color::Black);

        //
        // draw the rectangles here
        //

        //
        // end the current frame and display the window
        //
        win.display();
    } // while

    return 0;
} // main

