// vectors.cpp

#include <iostream>
#include <vector>
#include <string>

using namespace std;

void example1() {
	vector<string> words;
	int char_count = 0;
	string w;
	while (cin >> w) {
		char_count += w.size(); // doesn't count spaces
		words.push_back(w);
	}
	cout << "# of non-whitespace chars: " << char_count << "\n";
	cout << "               # of words: " << words.size() << "\n";
}

void example2() {
	vector<int> nums = {2, 10, 3, -9};

	// print the numbers
	for (int i = 0; i < nums.size(); i++) {
		cout << nums[i] << " ";
	}
	cout << "\n";

	// sum using a for-loop
	int total = 0.0;
	for (int i = 0; i < nums.size(); i++) {
		total += nums[i];
	}
	cout << "Sum: " << total << "\n";

	// sum using for-each loop
	total = 0.0;
	for(int x : nums) {
		total += x;
	}
	cout << "Sum: " << total << "\n";
}

// v is passed by constant reference, so no copy is made (and the compiler
// guarantees v is not modified inside the body of sum)
int sum(const vector<int>& v) {
	int result = 0.0;
	for (int i = 0; i < v.size(); i++) {
		result += v[i];
	}
	return result;
}

void example3() {
	vector<int> nums = {2, 10, 3, -9};

	// print the numbers
	for (int i = 0; i < nums.size(); i++) {
		cout << nums[i] << " ";
	}
	cout << "\n";

	cout << "Sum: " << sum(nums) << "\n";
}

// Converts a vector of ints to a human-readable string. The to_string
// function used inside the body is a standard C++ function for converting a
// number to a string.
string to_str(const vector<int>& v) {
	if (v.size() == 0) return "{}";
	if (v.size() == 1) return "{" + to_string(v[0]) + "}";

	// at this point we know v.size() > 1
	string result = "{" + to_string(v[0]);
	for (int i = 1; i < v.size(); i++) {  // i starts at 1, not 0
		result += ", " + to_string(v[i]);
	}
	return result + "}";
}

void example4() {
	vector<int> nums = {2, 10, 3, -9};

	// print the numbers
	cout << to_str(nums) << "\n";
	cout << "Sum: " << sum(nums) << "\n";
}

// C++ lets you create a << operator for any type of data. It must follow
// format, i.e. return an ostream&, and take an ostream& as the first
// parameter the value to be printed as the second parameter.
ostream& operator<<(ostream& out, const vector<int>& v) {
	out << to_str(v);
	return out;
}

void example5() {
	vector<int> nums = {2, 10, 3, -9};

	// print the numbers and their sum
	cout << nums 
         << "\n"
         << "Sum: " << sum(nums) 
         << "\n";
}

// The auto keyword tells the C++ compiler to *infer* the type of nums from
// the values being assigned to it. Here, nums is inferred to have the type
// vector<int>.
void example6() {
	auto nums = {2, 10, 3, -9};

	// print the numbers and their sum
	cout << nums 
	     << "\n"
         << "Sum: " << sum(nums) 
         << "\n";
}

int main() {
	// example1();
	example2();
	example3();
	example4();
	example5();
	example6();
}
